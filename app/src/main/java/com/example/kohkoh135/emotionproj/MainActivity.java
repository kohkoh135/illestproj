
package com.example.kohkoh135.emotionproj;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.*;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DBManager dbManager = new DBManager(getApplicationContext(), "Emotion.db", null, 2);

        Button happy_butn = (Button) findViewById(R.id.happy);
        Button good_butn = (Button) findViewById(R.id.good);
        Button well_butn = (Button) findViewById(R.id.well);
        Button notbad_butn = (Button) findViewById(R.id.notbad);
        Button soso_butn = (Button) findViewById(R.id.soso);
        Button surprised_butn = (Button) findViewById(R.id.surprised);
        Button bad_butn = (Button) findViewById(R.id.bad);
        Button terrible_butn = (Button) findViewById(R.id.terrible);
        Button fuck_butn = (Button) findViewById(R.id.fuck);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        happy_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("happy", sdf.format(date));
                System.out.print(dbManager.PrintData());
                Toast toast = Toast.makeText(MainActivity.this, "happy", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        good_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("good", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "good", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        well_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("well", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "well", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        notbad_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-tt-HHmm");

                dbManager.insert("not bad", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "not bad", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        soso_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("so so", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "so so", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        surprised_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-Hhmm");

                dbManager.insert("surprised", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "surprised", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        terrible_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("terrible", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "terrible", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        bad_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("bad", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "bad", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        fuck_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmm");

                dbManager.insert("fuck", sdf.format(date));
                Toast toast = Toast.makeText(MainActivity.this, "fuck", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

       /* Button join_butn = (Button)findViewById(R.id.login_join);
        join_butn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
*/
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_graph) {
            Intent intent = new Intent(MainActivity.this, GraphActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_setting) {
            /*Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);*/

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_store) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.kohkoh135.emotionproj/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.kohkoh135.emotionproj/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
