package com.example.kohkoh135.emotionproj;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.neokree.materialtabs.MaterialTab;
import com.neokree.materialtabs.MaterialTabHost;
import com.neokree.materialtabs.MaterialTabListener;

/**
 * 툴바에 탭을 설정하는 방법을 알 수 있습니다.
 *
 * @author Mike
 */
public class GraphActivity extends AppCompatActivity {

    ViewPager pager;
    ViewPagerAdapter pagerAdapter;
    MaterialTabHost tabhost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph);

        tabhost = (MaterialTabHost) this.findViewById(R.id.tabhost);
        pager = (ViewPager) this.findViewById(R.id.pager);

        // 뷰페이저 어댑터를 만듭니다.
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabhost.setSelectedNavigationItem(position);
            }
        });

        // 탭의 글자색을 지정합니다.
        tabhost.setTextColor(Color.WHITE);

        // 탭의 배경색을 지정합니다.
        tabhost.setPrimaryColor(Color.BLACK);

        // 탭을 추가합니다.
        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            MaterialTab tab = tabhost.newTab();
            tab.setText(pagerAdapter.getPageTitle(i));
            tab.setTabListener(new ProductTabListener());

            tabhost.addTab(tab);
        }

        // 처음 선택된 탭을 지정합니다.
        tabhost.setSelectedNavigationItem(0);
    }

    /**
     * 뷰페이저 어댑터를 정의합니다.
     */
    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int index) {
            Fragment frag = null;

            if (index == 0) {
                frag = new GraphFragment01();
            } else if (index == 1) {
                frag = new GraphFragment02();
            } else if (index == 2) {
                frag = new GraphFragment03();
            } else if (index == 3) {
                frag = new GraphFragment04();
            }

            return frag;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position) {
                case 0: return "DAY 그래프";
                case 1: return "WEEK 그래프";
                case 2: return "MONTH 그래프";
                case 3: return "검색";
                default: return null;
            }
        }

    }

    /**
     * 탭을 선택했을 때 처리할 리스너 정의
     */
    private class ProductTabListener implements MaterialTabListener {

        public ProductTabListener() {

        }

        @Override
        public void onTabSelected(MaterialTab tab) {
            pager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabReselected(MaterialTab tab) {

        }

        @Override
        public void onTabUnselected(MaterialTab tab) {

        }

    }

}
